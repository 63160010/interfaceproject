/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Acer
 */
public class Angleworm extends Reptile{
    private String nickname;
    public Angleworm(String nickname){
        super("Angleworm",0);
        this.nickname = nickname;
    }
    @Override
    public void crawl() {
        System.out.println("Angleworm: "+nickname+" crawl");
    }
    @Override
    public void eat() {
        System.out.println("Angleworm: "+nickname+" eat");
    }
    @Override
    public void speak() {
        System.out.println("Angleworm: "+nickname+" speak");
    }
    @Override
    public void sleep() {
        System.out.println("Angleworm: "+nickname+" sleep");
    }
}
