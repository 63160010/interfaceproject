/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Acer
 */
public class TestFlyable {
     public static void main(String[] args) {
         Bat bat =new Bat("Bo");
         Plane plane=new Plane("Engine number 1");
         bat.fly();
         plane.fly();
         Dog dog=new Dog("Dang");
         Car car=new Car("Engine number 1");
         car.run();
         Cat cat =new Cat("Kity");
         cat.run();
         
         
         Flyable[]flyables ={bat,plane};
         for(Flyable f : flyables){
             if(f instanceof Plane){
                 Plane p =(Plane)f;
                 p.startEngine();
                 p.run();
             }
             f.fly();
         }
         Runable[]runables ={dog,plane,car,cat};
         for(Runable r: runables){
             r.run();
         }
     }
}
