/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Acer
 */
public class Bird extends Poultry{
    private String nickname;
    public Bird(String nickname){
        super("Bird");
        this.nickname = nickname;
    }
    @Override
    public void fly() {
        System.out.println("Bird: "+nickname+" fly");
    }
    @Override
    public void eat() {
        System.out.println("Bird: "+nickname+" eat");
    }
    @Override
    public void speak() {
        System.out.println("Bird: "+nickname+" speak");
    }
    @Override
    public void sleep() {
        System.out.println("Bird: "+nickname+" sleep");
    }
}
